C_FLAGS= -Wall -Wpointer-arith -O2
CXX = g++ -std=c++1y
INC= -I. -I./v1.25_20140703/include
LIB= -L./v1.25_20140703/lib/linux64/ -lmduserapi -lboost_program_options
SRC_DIR= ./
OBJ_DIR= obj/
BIN_DIR= bin/
SOURCES_FILES_CPP=$(wildcard $(SRC_DIR)*.cpp)
OBJECTS = $(patsubst %.cpp,$(OBJ_DIR)%.o,$(notdir $(SOURCES_FILES_CPP)))

BIN_TARGET = $(BIN_DIR)SimpleShfeQd

OBJ_EXT= .o
CXXSRC_EXT= .cpp

$(OBJ_DIR)%$(OBJ_EXT): $(SRC_DIR)%$(CXXSRC_EXT)
	@echo "Compiling\t $< \t===>\t $@"
	@mkdir -p $(OBJ_DIR)
	@$(CXX) $(INC) $(C_FLAGS) -c $< -o $@

$(BIN_TARGET): $(OBJECTS)
	@echo "Linking\t\t $(OBJECTS) \t===>\t $@"
	@mkdir -p $(BIN_DIR)
	@$(CXX) $(C_FLAGS) $(INC) $(OBJECTS) -o $@ $(LIB) 

all: $(BIN_TARGET)
clean:
	@rm -rf $(BIN_TARGET) $(OBJECTS)

